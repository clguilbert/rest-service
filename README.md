(Projet importé depuis GitHub)

API intermédiaire utilisant les données d'OpenFood Facts pour calculer des scores nutritionnels à partir de code barres de produits. Il est possible de composer des paniers de produits à partir du endpoint http://localhost:8080/panier/[id_panier]/[code_barre].

Le endpoint pour une utilisation simple d'aggrégation depuis OpenFood Facts est : http://localhost:8080/off/[code_barre].